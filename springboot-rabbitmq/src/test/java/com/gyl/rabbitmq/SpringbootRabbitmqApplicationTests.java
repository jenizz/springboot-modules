package com.gyl.rabbitmq;

import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.service.ISendMessageSerivice;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootRabbitmqApplicationTests {
    @Autowired
    ISendMessageSerivice sendMessageSerivice;
    @Autowired
    RabbitTemplate rabbitTemplate;

    public static void main(String[] args) {

    }

    @Test
    void contextLoads() {
        Student student = new Student();

        student.setName("测试");
        student.setAge(19);
        for (int i = 0; i < 10; i++) {
            student.setId(i+"");
            sendMessageSerivice.sendWorkMessage(student);
        }

    }

}
