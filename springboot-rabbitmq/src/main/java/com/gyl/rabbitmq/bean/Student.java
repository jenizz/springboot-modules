package com.gyl.rabbitmq.bean;


import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gyl
 * @date 2020/9/11 - 10:39
 */
@Data
@NoArgsConstructor
public class Student implements Serializable {
    private String id;
    private String name;
    private Integer age;
}
