package com.gyl.rabbitmq.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gyl
 * @date 2020/9/11 - 13:57
 */
@Data
@NoArgsConstructor
public class RoutingKeyStudent extends Student {
    private String routingKey;
}
