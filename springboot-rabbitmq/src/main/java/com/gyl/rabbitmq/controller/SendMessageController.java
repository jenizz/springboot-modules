package com.gyl.rabbitmq.controller;

import com.gyl.rabbitmq.bean.RoutingKeyStudent;
import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.service.ISendMessageSerivice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author gyl
 * @date 2020/9/11 - 10:32
 */
@RestController
public class SendMessageController
{
    @Autowired
    ISendMessageSerivice sendMessageSerivice;

    @PostMapping("work")
    public String work(Student student){
        for (int i = 0; i < 10; i++) {
            student.setId(i+"");
            sendMessageSerivice.sendWorkMessage(student);
        }
        return "work ok";
    }
    @PostMapping("fanout")
    public String fanout(Student student){
        for (int i = 0; i < 10; i++) {
            student.setId(i+"");
            sendMessageSerivice.sendFanoutMessage(student);
        }
        return "fanout ok";
    }
    @PostMapping("direct")
    public String direct(RoutingKeyStudent routingKeyStudent){
        for (int i = 0; i < 10; i++) {
            routingKeyStudent.setId(i+"");
            sendMessageSerivice.sendDirectMessage(routingKeyStudent);
        }
        return "direct ok";
    }
    @PostMapping("topic")
    public String topic(RoutingKeyStudent routingKeyStudent){
        for (int i = 0; i < 10; i++) {
            routingKeyStudent.setId(i+"");
            sendMessageSerivice.sendTopicMessage(routingKeyStudent);
        }
        return "topic ok";
    }
}
