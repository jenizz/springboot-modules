package com.gyl.rabbitmq.constant;

/**
 * @author gyl
 * @date 2020/9/11 - 10:42
 */
public class Constants {
    // work模式队列
    public static final String WORK_QUEUE = "work_queue";
    // fanout 模式
    public static final String FANOUT_QUEUE = "fanout_queue";
    public static final String FANOUT_QUEUE_NEW = "fanout_queue_new";
    public static final String FANOUT_EXCHANGE = "fanout_exchange";
    // direct模式
    public static final String DIRECT_EXCHANGE = "direct_exchange";
    public static final String DIRECT_QUEUE = "direct_queue";
    public static final String DIRECT_QUEUE_NEW = "direct_queue_new";
    public static final String DIRECT_ROUTING_KEY_INFO = "student.info";
    public static final String DIRECT_ROUTING_KEY_ERROR = "student.error";


    // topic模式
    public static final String TOPIC_EXCHANGE = "topic_exchange";
    public static final String TOPIC_QUEUE = "topic_queue";
    public static final String TOPIC_QUEUE_NEW = "topic_queue_new";
    public static final String TOPIC_ROUTING_KEY_JIN = "student.#";
    public static final String TOPIC_ROUTING_KEY_XING = "student.*";
}
