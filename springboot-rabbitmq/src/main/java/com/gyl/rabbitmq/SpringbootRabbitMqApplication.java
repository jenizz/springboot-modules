package com.gyl.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author gyl
 */
@SpringBootApplication
public class SpringbootRabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRabbitMqApplication.class, args);
    }

}
