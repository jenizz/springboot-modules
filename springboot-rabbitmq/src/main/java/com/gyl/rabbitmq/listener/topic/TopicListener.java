package com.gyl.rabbitmq.listener.topic;

import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.constant.Constants;
import com.rabbitmq.client.Channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author gyl
 * @date 2020/9/13 - 16:46
 */
@Component
public class TopicListener {
    @RabbitListener(queuesToDeclare = @Queue(Constants.TOPIC_QUEUE))
    public void process(Student student, Channel channel, Message message) throws IOException {
        System.out.println("topic 消费者student.#: "+student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
    @RabbitListener(queuesToDeclare = @Queue(Constants.TOPIC_QUEUE_NEW))
    public void processNew(Student student, Channel channel, Message message) throws IOException {
        System.out.println("topic 消费者student.*: "+student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
