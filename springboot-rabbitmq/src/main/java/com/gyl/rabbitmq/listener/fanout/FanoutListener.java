package com.gyl.rabbitmq.listener.fanout;

import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.constant.Constants;
import com.rabbitmq.client.Channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author gyl
 * @date 2020/9/13 - 16:54
 */
@Component
public class FanoutListener {
    @RabbitListener(queuesToDeclare = @Queue(Constants.FANOUT_QUEUE))
    public void process(Student student, Channel channel, Message message) throws IOException {
        System.out.println("fanout消费者1: "+student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
    @RabbitListener(queuesToDeclare = @Queue(Constants.FANOUT_QUEUE_NEW))
    public void processNew(Student student, Channel channel, Message message) throws IOException {
        System.out.println("fanout 消费者2: "+student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
