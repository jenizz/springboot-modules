package com.gyl.rabbitmq.listener.direct;

import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.constant.Constants;
import com.rabbitmq.client.Channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author gyl
 * @date 2020/9/13 - 16:56
 */
@Component
public class DirectListener {
    @RabbitListener(queuesToDeclare = @Queue(Constants.DIRECT_QUEUE))
    public void process(Student student, Channel channel, Message message) throws IOException {
        System.out.println("direct消费者student.info: "+student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
    @RabbitListener(queuesToDeclare = @Queue(Constants.DIRECT_QUEUE_NEW))
    public void processNew(Student student, Channel channel, Message message) throws IOException {
        System.out.println("direct消费者student.error: "+ student);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
