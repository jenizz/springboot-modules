package com.gyl.rabbitmq.listener.work;

import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.constant.Constants;
import com.rabbitmq.client.Channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2020/9/13 - 16:38
 */
@Component
@Slf4j
public class WorkListener {
    @RabbitListener(queuesToDeclare = @Queue(Constants.WORK_QUEUE))
    public void process(Student student, Channel channel, Message message){
        try {
            System.out.println("work消费者1:  "+student);
            Thread.sleep(1000);
            // 返回确认状态
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException e) {
            log.error("发送消息错误",e.getMessage());
        } catch (InterruptedException e) {
            log.error("线程睡眠出现错误", e);
        }
    }
    @RabbitListener(queuesToDeclare = @Queue(Constants.WORK_QUEUE))
    public void processNew(Student student, Channel channel, Message message){
        try {
            System.out.println("work消费者2: "+student);
            // 返回确认状态
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (IOException e) {
            log.error("发送消息错误",e.getMessage());
        }

    }
}
