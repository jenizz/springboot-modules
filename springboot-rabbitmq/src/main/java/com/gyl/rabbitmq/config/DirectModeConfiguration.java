package com.gyl.rabbitmq.config;

import com.gyl.rabbitmq.constant.Constants;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author gyl
 * @date 2020/9/11 - 11:53
 * direct模式配置 需要交换机以及队列绑定,而且需要路由key来进行绑定
 */
@Configuration
public class DirectModeConfiguration {
    /**
     * direct 模式exchange
     * @return
     */
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(Constants.DIRECT_EXCHANGE);
    }

    /**
     * direct 队列
     * @return
     */
    @Bean
    public Queue directQueue(){
        return new Queue(Constants.DIRECT_QUEUE);
    }

    /**
     * 另一个队列
     * @return
     */
    @Bean
    public Queue directQueueNew(){
        return new Queue(Constants.DIRECT_QUEUE_NEW);
    }

    /**
     * 绑定交换机与队列
     * 只要是发送的消息携带路由键为 Constants.DIRECT_ROUTING_KEY_INFO，才对分发到此队列
     * @return
     */
    @Bean
    public Binding directBindingInfoMessage(){
        return BindingBuilder.bind(directQueue()).to(directExchange()).with(Constants.DIRECT_ROUTING_KEY_INFO);
    }

    /**
     * 绑定交换机与另一个队列
     * 只要是发送的消息携带路由键为 Constants.DIRECT_ROUTING_KEY_ERROR，才对分发到此队列
     * @return
     */
    @Bean
    public Binding directBindingErrorMessage(){
        return BindingBuilder.bind(directQueueNew()).to(directExchange()).with(Constants.DIRECT_ROUTING_KEY_ERROR);
    }

}
