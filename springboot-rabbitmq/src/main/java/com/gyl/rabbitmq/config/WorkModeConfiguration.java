package com.gyl.rabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author gyl
 * @date 2020/9/11 - 10:33
 * 工作模式 不需要绑定交换机，只需要绑定队列
 */
@Configuration
public class WorkModeConfiguration {
    /**
     * 生成一个名为workQueue的队列
     * @return
     */
    @Bean
    public Queue workQueue(){
        return new Queue("work_queue");
    }
}
