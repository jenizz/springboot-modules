package com.gyl.rabbitmq.config;

import com.gyl.rabbitmq.constant.Constants;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author gyl
 * @date 2020/9/11 - 11:29
 * 广播模式配置文件 需要配置交换机以及队列，并将其进行绑定
 */
@Configuration
public class FanoutModeConfiguration {

    /**
     * fanout模式交换机
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange(){
       return new FanoutExchange(Constants.FANOUT_EXCHANGE);
    }

    /**
     * fanout 模式队列1
     * @return
     */
    @Bean
    public Queue fanoutQueue(){
        return new Queue(Constants.FANOUT_QUEUE);
    }

    /**
     * fanout模式队列2
     * @return
     */
    @Bean
    public Queue fanoutQueueNew(){
        return new Queue(Constants.FANOUT_QUEUE_NEW);
    }

    /**
     * exchange以及queue绑定消息
     * @return
     */
    @Bean
    public Binding fanoutBindingMessage(){
        return BindingBuilder.bind(fanoutQueue()).to(fanoutExchange());
    }

    /**
     * exchange 以及另一个queue绑定消息
     * @return
     */
    @Bean
    public Binding fanoutBindingMessageNew(){
        return BindingBuilder.bind(fanoutQueueNew()).to(fanoutExchange());
    }
}
