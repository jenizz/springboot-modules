package com.gyl.rabbitmq.config;

import com.gyl.rabbitmq.constant.Constants;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author gyl
 * @date 2020/9/11 - 13:49
 * topic模式配置，需要交换机以及队列进行绑定，通过routing key
 * 和direct模式差不多，只是routing key可以通过匹配符进行匹配
 */
@Configuration
public class TopicModeConfiguration {
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(Constants.TOPIC_EXCHANGE);
    }
    @Bean
    public Queue topicQueue(){
        return new Queue(Constants.TOPIC_QUEUE);
    }
    @Bean
    public Queue topicQueueNew(){
        return new Queue(Constants.TOPIC_QUEUE_NEW);
    }

    /**
     * 只要是发送的消息携带路由键以student.# 开头后0个或多个单词的，都会分发到此队列
     * @return
     */
    @Bean
    public Binding topicBindingMessage(){
        return BindingBuilder.bind(topicQueue()).to(topicExchange()).with(Constants.TOPIC_ROUTING_KEY_JIN);
    }

    /**
     * 发送的消息携带路由键为student.* ,student后只能是1个单词,就会分发到此队列
     * @return
     */
    @Bean
    public Binding topicBindingNewMessage(){
        return BindingBuilder.bind(topicQueueNew()).to(topicExchange()).with(Constants.TOPIC_ROUTING_KEY_XING);
    }
}
