package com.gyl.rabbitmq.service;

import com.gyl.rabbitmq.bean.RoutingKeyStudent;
import com.gyl.rabbitmq.bean.Student;

/**
 * @author gyl
 * @date 2020/9/11 - 10:37
 */
public interface ISendMessageSerivice {

    /**
     * work模式发送信息
     * @param student
     */
    public void sendWorkMessage(Student student);

    /**
     * fanout 模式发送消息
     * @param student
     */
    public void sendFanoutMessage(Student student);

    /**
     * direct 模式发送消息
     * @param routingKeyStudent
     */
    public void sendDirectMessage(RoutingKeyStudent routingKeyStudent);

    /**
     * topick 模式发送消息
     * @param routingKeyStudent
     */
    public void sendTopicMessage(RoutingKeyStudent routingKeyStudent);
}
