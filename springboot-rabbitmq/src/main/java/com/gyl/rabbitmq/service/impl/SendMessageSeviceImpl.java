package com.gyl.rabbitmq.service.impl;

import com.gyl.rabbitmq.bean.RoutingKeyStudent;
import com.gyl.rabbitmq.bean.Student;
import com.gyl.rabbitmq.constant.Constants;
import com.gyl.rabbitmq.service.ISendMessageSerivice;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author gyl
 * @date 2020/9/11 - 10:37
 */
@Service
public class SendMessageSeviceImpl implements ISendMessageSerivice {
    @Autowired
    RabbitTemplate rabbitTemplate;
    /**
     * work模式发送信息
     * @param student
     */
    @Override
    public void sendWorkMessage(Student student) {
        rabbitTemplate.convertAndSend(Constants.WORK_QUEUE,student);
    }

    /**
     * fanout模式发送信息
     * @param student
     */
    @Override
    public void sendFanoutMessage(Student student) {
        rabbitTemplate.convertAndSend(Constants.FANOUT_EXCHANGE, "", student);
    }

    /**
     * direct模式发送消息
     * @param routingKeyStudent
     */
    @Override
    public void sendDirectMessage(RoutingKeyStudent routingKeyStudent) {
        Student student = new Student();
        student.setId(routingKeyStudent.getId());
        student.setAge(routingKeyStudent.getAge());
        student.setName(routingKeyStudent.getName());
        rabbitTemplate.convertAndSend(Constants.DIRECT_EXCHANGE, routingKeyStudent.getRoutingKey(),student);
    }

    /**
     * topic模式发送消息
     * @param routingKeyStudent
     */
    @Override
    public void sendTopicMessage(RoutingKeyStudent routingKeyStudent) {
        Student student = new Student();
        student.setId(routingKeyStudent.getId());
        student.setAge(routingKeyStudent.getAge());
        student.setName(routingKeyStudent.getName());
        rabbitTemplate.convertAndSend(Constants.TOPIC_EXCHANGE, routingKeyStudent.getRoutingKey(), student);
    }
}
