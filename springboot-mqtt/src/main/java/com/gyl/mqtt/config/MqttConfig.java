package com.gyl.mqtt.config;

import net.dreamlu.iot.mqtt.codec.ByteBufferUtil;
import net.dreamlu.iot.mqtt.codec.MqttVersion;
import net.dreamlu.iot.mqtt.core.client.MqttClient;
import net.dreamlu.iot.mqtt.core.server.MqttServer;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

/**
 * mqtt配置类
 * @author gyl
 * @date 2022/5/9 - 18:13
 */
@Configuration
@Slf4j
public class MqttConfig {
    /**
     * 注册一个客户端
     * @return
     */
    @Bean
    @ConditionalOnClass(MqttServer.class)
    public MqttClient mqttClient(){
        // 初始化 mqtt 客户端
        MqttClient client = MqttClient.create()
                .ip("127.0.0.1")
                .port(3883)
                .username("admin")
                .password("123456")
                .version(MqttVersion.MQTT_5)
                .connect();
        client.subQos0("/test/#", (topic, payload) -> {
            System.out.println("测试:"+ topic + ByteBufferUtil.toString(payload));
        });
        return client;
    }
}
