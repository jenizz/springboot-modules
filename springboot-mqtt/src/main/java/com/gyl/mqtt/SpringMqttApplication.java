package com.gyl.mqtt;

import net.dreamlu.iot.mqtt.core.server.MqttServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @author gyl
 * @date 2022/5/9 - 18:21
 */
@SpringBootApplication
public class SpringMqttApplication {
    private static MqttServer mqttServer;
    @Autowired
    public void setMqttServer(MqttServer mqttServer){
        SpringMqttApplication.mqttServer = mqttServer;
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringMqttApplication.class,args);
        try {
            System.out.println("等待mqtt服务器启动完成");
            Thread.sleep(1000*10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String body = "mqtt";
        mqttServer.publishAll("/test/"+body, ByteBuffer.wrap(body.getBytes(StandardCharsets.UTF_8)));
    }
}
