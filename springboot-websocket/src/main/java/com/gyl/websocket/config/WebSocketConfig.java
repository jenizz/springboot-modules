package com.gyl.websocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * websocket配置
 * @author gyl
 * @date 2022/5/12 - 15:17
 */
@Configuration
@Import(WebSocketProperties.class)
public class WebSocketConfig{
    /**
     * 注册一个扫描ServerEndpoint注解处理器
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }
}
