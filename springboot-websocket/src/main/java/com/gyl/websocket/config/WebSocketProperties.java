package com.gyl.websocket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author gyl
 * @date 2022/5/12 - 18:01
 */
@Configuration
@ConfigurationProperties(prefix = "websocket")
@Data
@EnableConfigurationProperties
public class WebSocketProperties {
    /**
     * 服务器url
     */
    private String url;
}
