package com.gyl.websocket.client;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/12 - 18:10
 */
@ClientEndpoint
@Slf4j
public class WebSocketClient {
    @OnOpen
    public void open(Session s) {
        log.info("客户端开启了");
    }
    @OnClose
    public void close(CloseReason c) {
        log.info("客户端关闭了"+ c.getReasonPhrase());

    }
    @OnError
    public void error(Throwable t) {
        log.info("客户端发生错误");
    }
    @OnMessage
    public void message(String message, Session session) {
        log.info("客户端获取到服务端的信息:"+ message);
    }


}
