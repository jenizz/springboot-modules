package com.gyl;


import com.gyl.netty.annotation.EnableNetty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/7 - 16:31
 */
@SpringBootApplication
@Slf4j
@EnableNetty
@EnableWebSocket
public class SpringBootExampleApplication {
    public static void main(String[] args){
        SpringApplication.run(SpringBootExampleApplication.class, args);
    }
}
