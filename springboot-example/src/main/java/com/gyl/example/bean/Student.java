package com.gyl.example.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gyl.aspect.annotation.FieldNotNull;
import com.gyl.aspect.annotation.FieldPatternLimited;
import com.gyl.common.enums.ErrorEnum;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author gyl
 * @date 2022/5/7 - 18:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Student {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @FieldPatternLimited(pattern = "^\\d{1,3}$",notMatchError = ErrorEnum.PARAM_NULL,subErrMsg = "年龄类型不正确")
    private Integer age;
    @FieldNotNull(nullError = ErrorEnum.PARAM_NULL,subErrMsg = "姓名不能为空")
    private String name;
    @TableField(fill = FieldFill.INSERT,value = "now()")
    private Date createTime;
    public Student(Integer age,String name){
        this.age = age;
        this.name = name;
    }
}
