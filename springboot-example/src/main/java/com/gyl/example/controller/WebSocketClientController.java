package com.gyl.example.controller;

import com.gyl.common.bean.AjaxResult;
import com.gyl.websocket.config.WebSocketClientConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.util.RandomUtil;

/**
 * @author gyl
 * @date 2022/5/12 - 18:18
 */
@RestController
@RequestMapping("websocket")
@CrossOrigin
public class WebSocketClientController {
    /**
     * 发送信息
     */
    @Autowired
    WebSocketClientConfig myWebSocketClientConfig;
    @GetMapping("sendMsg")
    public AjaxResult sendMsg(String msg,String userName) throws Exception {
        myWebSocketClientConfig.sendMsg(msg, userName);
        return AjaxResult.success();
    }

    /**
     * 模拟获取用户名
     * @return
     */
    @GetMapping("getUserName")
    public AjaxResult getUserName(){
        return AjaxResult.success(RandomUtil.randomString(5));
    }
}
