package com.gyl.example.controller;


import com.gyl.aspect.annotation.WebMethodVerify;
import com.gyl.common.bean.AjaxResult;
import com.gyl.example.bean.Student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  学生controller
 * </p>
 *
 * @author gyl
 * @since 2022-05-16
 */
@RestController
@RequestMapping("student")
public class StudentsController {
    static List<Student> studentList = new ArrayList<>();

    @GetMapping("list")
    public AjaxResult list(){
        return AjaxResult.success(studentList);
    }
    /**
     * 添加学生
     * @param student
     * @return
     */
    @WebMethodVerify("添加学生")
    @PostMapping("add")
    public AjaxResult add(@RequestBody Student student){
        studentList.add(student);
        return AjaxResult.success("添加学生成功");
    }
}
