package com.gyl.mybatisplus.mapper;

import com.gyl.mybatisplus.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gyl
 * @since 2022-05-16
 */
public interface StudentMapper extends BaseMapper<Student> {

}
