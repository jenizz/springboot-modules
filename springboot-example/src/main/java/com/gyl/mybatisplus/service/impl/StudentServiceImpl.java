package com.gyl.mybatisplus.service.impl;

import com.baomidou.dynamic.datasource.annotation.Master;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gyl.mybatisplus.entity.Student;
import com.gyl.mybatisplus.mapper.StudentMapper;
import com.gyl.mybatisplus.service.IStudentService;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gyl
 * @since 2022-05-16
 */
@Service
@Master
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

}
