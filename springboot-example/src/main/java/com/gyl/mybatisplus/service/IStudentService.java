package com.gyl.mybatisplus.service;

import com.gyl.mybatisplus.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gyl
 * @since 2022-05-16
 */
public interface IStudentService extends IService<Student> {

}
