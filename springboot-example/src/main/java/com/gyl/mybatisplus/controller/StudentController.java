package com.gyl.mybatisplus.controller;


import com.gyl.common.bean.AjaxResult;
import com.gyl.mybatisplus.entity.Student;
import com.gyl.mybatisplus.service.IStudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gyl
 * @since 2022-05-16
 */
@RestController
@RequestMapping("/mybatisplus/student")
public class StudentController {
    @Autowired
    IStudentService studentService;

    /**
     * 获取列表
     * @return
     */
    @GetMapping("list")
    public AjaxResult list(){
        return AjaxResult.success(studentService.list());
    }
    @PostMapping("add")
    public AjaxResult add(@RequestBody Student student){
        return AjaxResult.success(studentService.save(student));
    }
    @PostMapping("update")
    public AjaxResult update(@RequestBody Student student){
        return AjaxResult.success(studentService.updateById(student));
    }
    @PostMapping("delete")
    public AjaxResult delete(@RequestBody Student student){
        return AjaxResult.success(studentService.removeById(student));
    }


}
