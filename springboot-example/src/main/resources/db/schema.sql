CREATE TABLE if not exists  `student` (
                           `id` varchar(60) NOT NULL COMMENT '主键',
                           `name` varchar(60) NOT NULL COMMENT '姓名',
                           `age` int(10) NOT NULL COMMENT '年龄',
                           `create_time` datetime NOT NULL COMMENT '创建时间',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8