package com.gyl.example;

import com.baomidou.mybatisplus.core.toolkit.AES;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.gyl.SpringBootExampleApplication;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/16 - 11:24
 */
@SpringBootTest(classes = SpringBootExampleApplication.class)
@Slf4j
@RunWith(SpringRunner.class)
public class MybatisPlusTest {

    /**
     * 代码生成测试
     * @param args
     */
    public static void main(String[] args) {

        MybatisPlusTest mybatisPlusTest = new MybatisPlusTest();
        mybatisPlusTest.testDataEncrypt();

    }

    /**
     * 代码生成
     */
    public void autoGeneratorCode(){
        FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf-8&userSSL=false", "root", "root")
                .globalConfig(builder -> {
                    builder.author("gyl") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
//                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D://mydata//git//springboot-modules//springboot-example//src//main//java//"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.gyl") // 设置父包名
                            .moduleName("mybatisplus") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D://mydata//git//springboot-modules//springboot-example//src//main//resources//mapper//")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("student") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

    /**
     * 账号密码加密
     */
    public void testDataEncrypt(){
        // 随机数
        String randomKey = AES.generateRandomKey();
        randomKey = "98957fd9d1449a42";
        String url = "jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8";
        String userName = "root";
        String password = "root";
        String urlEncrypt = AES.encrypt(url, randomKey);
        String userNameEncrypt = AES.encrypt(userName, randomKey);
        String passwordEncrypt = AES.encrypt(password, randomKey);
        log.info("加密后随机数{}，url地址{},用户名{},密码{}",randomKey,urlEncrypt,userNameEncrypt,passwordEncrypt);
        String urlH2 = "jdbc:h2:~/test";
        String userNameH2 = "sa";
        String passwordH2 = "";
        String urlEncryptH2 = AES.encrypt(urlH2, randomKey);
        String userNameEncryptH2 = AES.encrypt(userNameH2, randomKey);
        String passwordEncryptH2 = AES.encrypt(passwordH2, randomKey);
        log.info("加密后随机数{}，url地址{},用户名{},密码{}",randomKey,urlEncryptH2,userNameEncryptH2,passwordEncryptH2);
    }
}
