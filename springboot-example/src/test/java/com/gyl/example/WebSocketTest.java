package com.gyl.example;

import com.gyl.SpringBootExampleApplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/12 - 16:51
 */
@SpringBootTest(classes = SpringBootExampleApplication.class)
@Slf4j
@RunWith(SpringRunner.class)
public class WebSocketTest {
    @Test
    public void testWebSocket(){

    }
}
