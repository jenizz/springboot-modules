package com.gyl.example;

import com.gyl.SpringBootExampleApplication;
import com.gyl.example.bean.Student;
import com.gyl.redis.utils.RedisUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import cn.hutool.core.map.MapUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/7 - 15:38
 */
@SpringBootTest(classes = SpringBootExampleApplication.class)
@Slf4j
@RunWith(SpringRunner.class)
public class RedisTest {
    /**
     * redis 数据库切换测试
     * 包括主数据库master以及备用数据库slave
     */
    @Test
    public void testRedis(){
        String STRING_KEY = "testString";
        String LIST_KEY = "testList";
        String HASH_KEY = "testHash";
        String SET_KEY = "testSet";
        RedisUtils redisUtils = new RedisUtils(true);
        RedisUtils redisUtilSalve = new RedisUtils(false);

        // string操作
        redisUtils.setObject(STRING_KEY,"test");
        redisUtilSalve.setObject(STRING_KEY,new Student(20,"小明"));
        // list操作
        redisUtils.setList(LIST_KEY, Arrays.asList(1,2));
        redisUtilSalve.setList(LIST_KEY,Arrays.asList(new Student(1,"1"),new Student(2,"2")));
        // map操作
        redisUtils.setMap(HASH_KEY, MapUtil.of(HASH_KEY,"test"));
        redisUtilSalve.setMap(HASH_KEY,MapUtil.of(HASH_KEY,"testSlave"));
        // set 操作
        HashSet hashSet = new HashSet();
        hashSet.add("test");
        hashSet.add("test1");
        redisUtils.setSet(SET_KEY,hashSet);
        hashSet.add("testSalve");
        redisUtilSalve.setSet(SET_KEY,hashSet);

        // 打印展示
        // string打印
        System.out.println("=======string打印======");
        System.out.println(redisUtils.getObject(STRING_KEY));
        Student student = redisUtilSalve.getObject(STRING_KEY, Student.class);
        System.out.println(student);
        System.out.println("=======list打印======");
        // list 打印
        System.out.println(redisUtils.getList(LIST_KEY));
        List<Student> studentList = redisUtilSalve.getList(LIST_KEY,Student.class);
        studentList.stream().forEach(System.out::println);
        System.out.println("=======map打印======");
        // map 打印
        redisUtils.getMap(HASH_KEY).forEach((k,v)->{
            System.out.println(k+":"+v);
        });
        redisUtilSalve.getMap(HASH_KEY).forEach((k,v)->{
            System.out.println(k+":"+v);
        });
        System.out.println("=======set打印======");
        // set 打印
        redisUtils.getSet(SET_KEY).forEach(v->{
            System.out.println(v);
        });
        redisUtilSalve.getSet(SET_KEY).forEach(v->{
            System.out.println(v);
        });
    }
}
