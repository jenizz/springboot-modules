package com.gyl.example;

import com.gyl.SpringBootExampleApplication;
import com.gyl.netty.bean.MessageBean;
import com.gyl.netty.client.ClientBoot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

/**
 * netty测试类
 * @author gyl
 * @date 2022/5/11 - 15:06
 */
@SpringBootTest(classes = SpringBootExampleApplication.class)
@Slf4j
@RunWith(SpringRunner.class)
public class NettyTest {
    @Autowired
    ClientBoot clientBoot;
    @Test
    public void clientTest() throws InterruptedException {
        // 创建连接
        clientBoot.sendMsg(new MessageBean("hello 服务器"));
    }
}
