package com.gyl.common.enums;

/**
 * 错误枚举类
 */
public enum ErrorEnum {
    NO_ERROR("0",""),
    PARAM_NULL("P11000", "必填参数为空");

    private String errorCode;
    private String errorMsg;

    ErrorEnum() {
    }

    ErrorEnum(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
