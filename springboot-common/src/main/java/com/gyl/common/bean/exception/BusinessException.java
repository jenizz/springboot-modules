package com.gyl.common.bean.exception;


import com.gyl.common.enums.ErrorEnum;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import lombok.Data;

/**
 * @Description: (业务逻辑异常)
 * @author gyl
 */
@Data
public class BusinessException extends RuntimeException {
	private static final Log log = LogFactory.get();
	private String code;
	private String errorMsg;

	public BusinessException(ErrorEnum resultEnum) {
		super(resultEnum.getErrorMsg());
		this.code = resultEnum.getErrorCode();
		this.errorMsg = resultEnum.getErrorMsg();
		log.info(this.code +":"+ resultEnum.getErrorMsg());
	}
	
	public BusinessException(String Code, String ErrorMsg) {
		super(ErrorMsg);
		this.code = Code;
		this.errorMsg = ErrorMsg;
		log.info(this.code +":"+ ErrorMsg);
	}

	public BusinessException(String ErrorMsg) {
		super(ErrorMsg);
		this.code = "C1000";
		this.errorMsg = ErrorMsg;
		log.info(this.code +":"+ ErrorMsg);
	}

	public BusinessException(ErrorEnum errorEnum, String subErrMsg) {
		super(errorEnum.getErrorMsg() + "，" + subErrMsg);
		this.code = errorEnum.getErrorCode();
		log.info(this.code +"："+ errorEnum.getErrorMsg() + "，" + subErrMsg);
	}
}