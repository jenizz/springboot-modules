package com.gyl.retry.config;

import com.gyl.retry.listener.MyRetryListener;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.support.RetryTemplate;


/**
 * 重试策略配置
 *
 * @author gyl
 * @date 2022/5/5 - 16:01
 */
@Configuration
public class RetryConfig {
    @Bean
    public RetryTemplate retryTemplate(MyRetryListener myRetryListener) {
        // 定义简易重试策略，最大重试次数为3次,重试间隔为3s
        RetryTemplate retryTemplate = RetryTemplate.builder()
                .maxAttempts(3)
                .fixedBackoff(3000)
                .retryOn(RuntimeException.class)
                .build();
        retryTemplate.registerListener(myRetryListener);
        return retryTemplate;
    }

}
