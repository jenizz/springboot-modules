package com.gyl.retry.service;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author gyl
 * @date 2022/5/5 - 15:42
 */
@Service
@Slf4j
public class CommonService {
    public void test(String before) {
        for (int i = 0; i < 10; i++) {
            if (i == 2) {
                log.error("{}:有异常哦，我再试多几次看下还有没异常", before);
                throw new RuntimeException();
            }
            log.info("{}:打印次数: {}", before, i + 1);
        }
    }

    public void recover(String before) {
        log.error("{}:还是有异常，程序有bug哦", before);
    }
}
