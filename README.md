# 前言
集成一些日常开发用到的框架，提供示例，方便后续二次开发。

---
# spring-example
> 集成模块示例
# spring-redis
> 封装redis常用操作工具并暴露给其他模块使用
> 博客链接：https://blog.csdn.net/weixin_41307800/article/details/124669915
# spring-retry
> 提供spring retry框架示例
> 博客链接：https://blog.csdn.net/weixin_41307800/article/details/124604598
# spring-rabbitmq
> 提供rabbitmq框架示例
> 博客链接: https://blog.csdn.net/weixin_41307800/article/details/108557483
# spring-mqtt
> 提供mqtt框架示例，包括客户端以及服务端
# springboot-common
> 提供公共工具类，实体类

# springboot-aspect
> 提供切面工具类

# springboot-netty
> 提供netty框架使用示例，包括客户端以及服务端
> 博客链接:https://blog.csdn.net/weixin_41307800/article/details/124729710




