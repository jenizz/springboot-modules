package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 数字类型的字段的值大小有限制(不含)	 
 * @author gyl
 * @date 2019/03/21 19:24:23
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NumberFieldValueLimited {

	/**
	 * 字段允许的最大值, 小于0表示不指定
	 */
	int maxValue() default -1;
	/**
	 * 超出最大值的报错信息
	 */
	ErrorEnum overMaxValueError() default ErrorEnum.NO_ERROR;
	/**
	 * 过大的错误信息
	 */
	String tooBigSubErrMsg() default "";
	
	/**
	 * 字段允许的最小值, 小于0表示不指定
	 */
	int minValue() default -1;
	/**
	 * 小于字段的最小值得报错信息
	 */
	ErrorEnum lessThanMinValueError() default ErrorEnum.NO_ERROR;

	/**
	 * 太小的错误信息
	 */
	String tooSmallSubErrMsg() default "";
}
