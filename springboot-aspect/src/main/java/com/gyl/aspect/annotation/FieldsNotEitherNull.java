package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @description	几个字段,至少有一个不为空
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldsNotEitherNull {

	/**
	 * 不允许全部为空的字段名的数组
	 * @return
	 */
	String[] fieldNames();							
	/**
	 * 一旦所有的字段都为空,报出的错误信息
	 * @return
	 */
	ErrorEnum bothNullError();
}
