package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description	在前限定条件下, 该字段不允许为空.
 * 		 前限定条件: 当preFieldName字段值为preFieldValue的时候.
 * 		 针对String类型的字段
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldNotNullIntDepends {

	/**
	 * 前限定字段
	 */
	String dependsFieldName();
	/**
	 * 前限定字段的值--当前限定字段等于该值时,本字段不允许为空
	 * @return
	 */
	int dependsFieldValue();
	/**
	 * 不符合前限定条件时, 报出的错误信息
	 */
	ErrorEnum nullError();
	
	/**
	 * 详细错误信息
	 */
	String subErrMsg();
}
