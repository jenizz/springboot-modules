package com.gyl.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @description 注解FieldNotNullByPreviousFieldValue的容器,
 * 当前限定条件为多个的时候, 就使用这个注解, 在该注解中可以放置多个前限制条件
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldNotNullDependsContainer {
	/**
	 * 多个前置条件
	 * @return
	 * @author gyl  @date 2019/03/08
	 */
	FieldNotNullDepends[] previousLimiteds();
	
}
