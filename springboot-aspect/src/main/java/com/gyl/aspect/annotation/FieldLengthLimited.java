package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段的长度有限制, 当字段值不为空的时候. 仅适用于String类型的字段
 *  限制准确长度
 *  限制最小长度
 *  限制最大长度    
 * @description
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldLengthLimited {

	/**
	 * 指定的字符串准确长度,长于或短于该长度都不行.
	 * 小于0表示不指定		
	 */
	int length() default -1;
	/**
	 * 字段长度大于或小于指定的长度,对应的错误信息
	 * @return
	 */
	ErrorEnum shorterOrLongerError() default  ErrorEnum.NO_ERROR;
	/**
	 * 详细错误信息  
	 */
	String shorterOrLongerSubErrMsg() default "";
	
	/**
	 * 该字符串属性的最大长度(含), 
	 * 小于0表示不指定
	 */
	int maxLength() default -1;									
	/**
	 * 一旦超出最大长度,对应的错误信息
	 */
	ErrorEnum tooLongError() default ErrorEnum.NO_ERROR;	
	/**
	 * 详细错误信息  
	 */
	String tooLongSubErrMsg() default "";
	
	/**
	 * 字段的最小长度, 小于0表示不指定
	 * @return
	 * @author gyl  @date 2019/03/14
	 */
	int minLength() default -1;
	/**
	 * 一旦长度小于最小长度报出的错误信息
	 * @return
	 * @author gyl  @date 2019/03/14
	 */
	ErrorEnum tooShortError() default ErrorEnum.NO_ERROR;
	
	/**
	 * 详细错误信息  
	 */
	String tooShortSubErrMsg() default "";
}
