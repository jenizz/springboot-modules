package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  字段不允许为空(空指针,空字符串,"null"字符串)
 * @description
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldNotNull {
	/**
	 * 如果为空,对应的错误信息
	 * @return
	 */
	ErrorEnum nullError() default ErrorEnum.PARAM_NULL;
	
	/**
	 * @description 详细的错误信息  
	 * @author gyl   @date 2019-12-13 09:51:56
	 */
	String subErrMsg();
}
