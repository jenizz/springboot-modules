package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  字段如果不为空, 它的的值就固定在一定的枚举值之内, 只用于String类型的字段
 * @description
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldValueLimited {

	/**
	 * 当该字段不为空的时候, 允许的值
	 * @return
	 */
	String[] limitedValues();			
	/**
	 * 一旦属性值不在给定的枚举值之内,对应的错误信息
	 * @return
	 */
	ErrorEnum overLimitedValueError();
	
	/**
	 * 详细错误信息
	 */
	String subErrMsg();
}
