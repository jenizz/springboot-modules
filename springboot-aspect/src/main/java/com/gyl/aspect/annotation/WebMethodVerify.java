package com.gyl.aspect.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * web方法校验注解
 * @author gyl
 * @date 2022/5/10 - 14:49
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface WebMethodVerify {
    /**
     * 方法名称
     * @return
     */
    String value();
}
