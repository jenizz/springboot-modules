package com.gyl.aspect.annotation;


import com.gyl.common.enums.ErrorEnum;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 字段的格式有限制--仅限于String类型的字段
 * 使用正则表达式来匹配
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldPatternLimited {

	/**
	 * 字符串的格式--正则表达式
	 * @return
	 * @author gyl  @date 2019/03/20
	 */
	String pattern();
	/**
	 * 一旦字段不匹配格式,抛出的异常
	 * @return
	 * @author gyl  @date 2019/03/20
	 */
	ErrorEnum notMatchError();
	
	/**
	 * 明细错误信息
	 */
	String subErrMsg();
}
