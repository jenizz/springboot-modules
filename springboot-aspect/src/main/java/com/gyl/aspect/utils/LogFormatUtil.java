package com.gyl.aspect.utils;


import com.gyl.common.utils.ExceptionUtil;
import com.gyl.common.utils.StringUtil;

import cn.hutool.json.JSONUtil;

/**
 * @description 生成打印到日志中的语句的工具类, 制定通用的日志打印格式
 */
public class LogFormatUtil {
    /**
     * 3个换行符，作为处理过程之间的分隔符
     */
    public static final String separator = "\n\n\n";
    /**
     * 20个
     */
    public static final String longBorder = "--------------------";

//	----------------------------------------处理请求---------------------------------------------------

    /**
     * @description 开始处理
     */
    public static String beforeHandle(String methodName, String uri, Object request, String requestId) {
        String json = "";
        try {
            json = JSONUtil.toJsonStr(request);
        } catch (Exception e) {

        }
        if (StringUtil.isNullOrEmptyAfterTrim(methodName)) {
            return StringUtil.append(longBorder, "处理请求开始，请求地址【", uri
                    , "】", longBorder, "\n请求参数【", json, "】requestId:【" + requestId + "】");
        }
        return StringUtil.append(longBorder, "处理请求开始，方法名【", methodName
                , "】", longBorder, "\n请求地址【", uri, "】\n请求参数【", json, "】requestId:【" + requestId + "】");
    }

    /**
     * @description 处理成功
     */
    public static String handleSucceed(String methodName, long timeInMs, Object response, String requestId) {
        String json = JSONUtil.toJsonStr(response);
        if (StringUtil.isNullOrEmptyAfterTrim(methodName)) {
            return StringUtil.append(longBorder, "处理请求成功，耗时【", String.valueOf(timeInMs)
                    , "】ms", longBorder, "\n响应参数【", json, "】requestId:【" + requestId + "】", separator);

        }
        return StringUtil.append(longBorder, "处理请求成功，方法名【", methodName, "】，耗时【", String.valueOf(timeInMs)
                , "】ms", longBorder, "\n响应参数【", json, "】requestId:【" + requestId + "】", separator);
    }
//	--------------------------------------------------------------过滤器----------------------------------------

    /**
     * @description 过滤之前
     */
    public static String beforeFilter(String filterName) {
        return StringUtil.append(longBorder, "【", filterName, "】开始过滤", longBorder);
    }

//	-------------------------------------一般操作统一格式--指定边框--------------------------------------------------

    /**
     * @description 任务开始前
     */
    public static String before(String taskName, String border) {
        return StringUtil.append(border, taskName, "开始执行！", border);
    }

    /**
     * @description 任务成功, 时间小于0, 表示不打印执行时间
     */
    public static String after(String taskName, long time, String border) {
        if (time >= 0) {
            return StringUtil.append(border, taskName, "执行完成，耗时【", String.valueOf(time), "】ms！", border);
        } else {
            return after(taskName, border);
        }
    }

    /**
     * @description 任务成功，不打印时间
     */
    public static String after(String taskName, String border) {
        return StringUtil.append(border, taskName, "执行完成！", border);
    }

    /**
     * @description 任务失败，以及失败的原因
     */
    public static String fail(String methodName, String failReason, String border) {
        return StringUtil.append(border, methodName, "执行失败！失败原因如下", border, "\n", failReason);
    }

    /**
     * @description 任务出现异常
     */
    public static String exception(String methodName, Exception e, String border) {
        return StringUtil.append(border, methodName, "执行失败！异常信息如下", border, "\n", ExceptionUtil.printStackTrace(e));
    }
}