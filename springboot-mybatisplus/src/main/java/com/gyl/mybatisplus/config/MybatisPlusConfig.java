package com.gyl.mybatisplus.config;

import com.gyl.mybatisplus.global.MySqlInjector;
import com.gyl.mybatisplus.handler.MyMetaObjectHandler;
import com.gyl.mybatisplus.wrapper.ExcludeEmptyQueryWrapper;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatisplus配置
 * @author gyl
 * @date 2022/5/13 - 15:24
 */
@Configuration
@MapperScan("com.gyl.**.mapper")
public class MybatisPlusConfig{
    /**
     * sql注入器
     * @return
     */
    @Bean
    MySqlInjector mySqlInjector(){
        return new MySqlInjector();
    }

    /**
     * 配合@TableField使用
     * @return
     */
    @Bean
    MyMetaObjectHandler myMetaObjectHandler(){
        return new MyMetaObjectHandler();
    }

    /**
     * 自定义wrapper类
     * @return
     */
    @Bean
    ExcludeEmptyQueryWrapper excludeEmptyQueryWrapper(){
        return new ExcludeEmptyQueryWrapper();
    }

}
